# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-22 01:54+0000\n"
"PO-Revision-Date: 2013-02-20 13:38+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: dnssd.cpp:47
#, kde-format
msgid "FTP servers"
msgstr "FTP सर्व्हर्स"

#: dnssd.cpp:48
#, kde-format
msgid "WebDav remote directory"
msgstr "WebDav दूरस्थ संचयीका"

#: dnssd.cpp:50
#, kde-format
msgid "Remote disk (sftp)"
msgstr "दूरस्थ डिस्क (sftp)"

#: dnssd.cpp:52
#, kde-format
msgid "Remote disk (fish)"
msgstr "दूरस्थ डिस्क (fish)"

#: dnssd.cpp:53
#, kde-format
msgid "NFS remote directory"
msgstr "NFS दूरस्थ संचयीका"

#: dnssd.cpp:151
#, kde-format
msgid "The Zeroconf daemon (mdnsd) is not running."
msgstr "झीरोकॉन्फ़ डिमन (एमडीएनएसडी) चालू नाही."

#: dnssd.cpp:153
#, fuzzy, kde-format
#| msgid "KDE has been built without Zeroconf support."
msgid "The KDNSSD library has been built without Zeroconf support."
msgstr "केडीई झीरोकॉन्फ़ च्या समर्थना शिवाय बिल्ड केलेले आहे."

# translation of kio_zeroconf.po to Occitan (lengadocian)
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_zeroconf\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-22 01:54+0000\n"
"PO-Revision-Date: 2008-08-05 22:27+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: dnssd.cpp:47
#, kde-format
msgid "FTP servers"
msgstr ""

#: dnssd.cpp:48
#, kde-format
msgid "WebDav remote directory"
msgstr ""

#: dnssd.cpp:50
#, kde-format
msgid "Remote disk (sftp)"
msgstr ""

#: dnssd.cpp:52
#, kde-format
msgid "Remote disk (fish)"
msgstr ""

#: dnssd.cpp:53
#, kde-format
msgid "NFS remote directory"
msgstr ""

#: dnssd.cpp:151
#, kde-format
msgid "The Zeroconf daemon (mdnsd) is not running."
msgstr ""

#: dnssd.cpp:153
#, kde-format
msgid "The KDNSSD library has been built without Zeroconf support."
msgstr ""

#~ msgid "Protocol name"
#~ msgstr "Nom del protocòl"
